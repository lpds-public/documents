# Container Queue

## Intro
- Simple queue service powered by Machinery.
- Workers execute commands in docker containers.

## Used open source components
- ### Machinery
    Machinery is an asynchronous task queue/job queue based on distributed message passing. - https://github.com/RichardKnop/machinery

- ### Rabbitmq
    RabbitMQ is the most widely deployed open source message broker. - https://www.rabbitmq.com

- ### Redis
    Redis is an open source, in-memory data structure store. - https://redis.io

## Crafted components
- ### Frontend
    Frontend provides a RESTful interface for the users.  

- ### Worker
    Worker fetches, dispatcheses and executes tasks in docker containers. It collects and sends logs and results.

## Usage
- ### Set up and launch containers
```bash
docker-compose up
```

- ### Push 'hello world' _job (available params: image string, env []string, cmd []string, container_name string)_
```bash
curl -H 'Content-Type: application/json' -X POST -d'{"image":"ubuntu", "cmd":["echo", "hello Docker"]}' http://localhost:8080/task
```

- ### Check status
```bash
curl -X GET http://localhost:8080/task/$(myUniqId)
```

- ### Fetch result
```bash
curl -X GET http://localhost:8080/task/$(myUniqId)/result
```

- ### Purge task
```bash
curl -X DELETE http://localhost:8080/task/$(myUniqId)
```

## Debug
Check container logs
```bash
docker logs -f $(containerID)
```


# SZTAKI OpenNebula cloud access for the COLA users

## Accessing the SZTAKI cloud directly
In this access mode, you are authorised to launch virtual machines under your personal account created for you. You will be able to login to the user interface (sunstone) of the SZTAKI OpenNebula cloud and will have full control over your virtual machines and personal settings. You can also launch virtual machines via EC2 interface under your account.

Quota restrictions per user: 
* 4 VMs (mapped on max 2 physical cores)
* 4 GB RAM
* No public IP address

## Accessing the SZTAKI cloud through CloudBroker
In this access mode, you initiate the creation of virtual machines under your account on the [COLA CloudBroker platform](https://cola-prototype.cloudbroker.com). The virtual machines will then be launched on the SZTAKI OpenNebula cloud under a predefined user account owned by CloudBroker.  

Quota restrictions (combined for all VMs created via CloudBroker):
* 16 VMs
* 16 GB RAM
* No public IP address

## How to login to the virtual machines
All virtual machines launched under the SZTAKI OpenNebula cloud for COLA users have only private ip addresses. To access your virtual machine, we provide OpenVPN access for the SZTAKI OpenNebula network to which the private ips belong to. For this purpose we provide a client OpenVPN configuration file. More details can be found below on this topic.

## Access request
You can get access to the SZTAKI OpenNebula cloud by sending a request by email to [devnull@lists.lpds.sztaki.hu](mailto://devnull@lists.lpds.sztaki.hu). The mail must contain the following information:
* surname
* family name
* company
* framework ("COLA project")
* access mode ("Direct" or "CloudBroker")

As an answer you will get the followings:
* For CloudBroker access mode:
  * OpenVPN configuration file (e.g., sztaki_opennebula.ovpn)
* For direct access mode:
  * Username/Password for SZTAKI OpenNebula
  * OpenVPN configuration file (e.g., sztaki_opennebula.ovpn)

## Technical parameters:
1. SZTAKI OpenNebula (Sunstone GUI): 
  * endpoint: https://opennebula.lpds.sztaki.hu
  * username/password: sent by email
2. SZTAKI OpenNebula (EC2 endpoint): 
  * endpoint: https://opennebula.lpds.sztaki.hu:4567
  * accesskey: equals to the username sent by email
  * secretkey: SHA1SUM of the password sent by email (see help below)
  * regionname: ""
  * image id: ami-00000450 (preferred Ubuntu 16.04)
  * instance types: e.g., t2.small 
3. CloudBroker details for VM creation:
  * endpoint: https://cola-prototype.cloudbroker.com
  * resource: "OpenNebula CloudBroker GmbH SZTAKI"
  * deployment: "Linux Ubuntu 14.04 Budapest x86_64 R1"
  * instance type: "OpenNebula CloudBroker GmbH SZTAKI m1.small"
  * region: "Budapest"
 
### Insance types:

| type | VCPU | Mem (GiB) |
| -------- | -------- | -------- |
| t1.nano  | 1 | 0.5  |
| t1.micro | 1 | 1 |
| t1.small | 1 | 2 |
| t1.medium| 2 | 4 |
| t1.large | 2 | 8 |

## Calculating the SHA1SUM of your password
* Linux
  * Type your password (sent by email) as input for the following command:
    * ```read -s pwd; echo -n $pwd | sha1sum | cut -f1 -d' '; unset pwd```
* Windows
  * Create a txt file with your password (e.g. c:\mypwd.txt)
  * Open a command prompt (cmd.exe)
  * Run the following commands:
    - ```powershell```
    - ```Get-FileHash C:\mypwd.txt -Algorithm SHA1 | Format-List```
  * Delete c:\mypwd.txt

## Using OpenVPN
* Linux
  * Install the openvpn package: e.g. ```apt-get install openvpn```
  * Start openvpn using the received openvpn config file (e.g., sztaki_opennebula.ovpn): ```openvpn ../sztaki_opennebula.ovpn```
* Windows
  * Download the OpenVPN client: https://swupdate.openvpn.org/community/releases/openvpn-install-2.4.4-I601.exe
  * Install the OpenVPN client. 
  * Import the received openvpn config file (e.g., sztaki_opennebula.ovpn), and connect.  

## Links
* [SZTAKI OpenNebula admin contact](devnull@lists.lpds.sztaki.hu)
* [COLA project](http://project-cola.eu)
* [LPDS OpenNebula Dashboard](https://opennebula.lpds.sztaki.hu)
* [OpenVPN Windows client](https://swupdate.openvpn.org/community/releases/openvpn-install-2.4.4-I601.exe)
* [OpenVPN ](https://openvpn.net/)
